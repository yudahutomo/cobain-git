    SUBROUTINE ATI.VVR.FT.MYH
*---------------------------------------------------------------------
* Developer Name     : YUDA
* Development Date   : 20211011
* Description        : Training Version Plus 2
*-----------------------------------------------------------------------------
* Modification History:-
*-----------------------------------------------------------------------------
* Date           	: 
* Modified by    	: 
* Description		: 
*-----------------------------------------------------------------------------
*
*-----------------------------------------------------------------------------
    $INSERT I_COMMON
    $INSERT I_EQUATE
	$INSERT I_F.FUNDS.TRANSFER
	$INSERT I_F.ACCOUNT
	
*-----------------------------------------------------------------------------
MAIN:
*-----------------------------------------------------------------------------
    GOSUB INIT
    GOSUB PROCESS

    RETURN
*---------------------------------------------------------------------
INIT:
*---------------------------------------------------------------------
	FN.ACCOUNT	= "F.ACCOUNT"
	F.ACCOUNT	= ""
	CALL OPF(FN.ACCOUNT,F.ACCOUNT)
	
    RETURN
*---------------------------------------------------------------------
PROCESS:
*---------------------------------------------------------------------
	Y.DEBIT.ACCT = COMI
	CALL F.READ(FN.ACCOUNT,Y.DEBIT.ACCT,R.ACCOUNT,F.ACCOUNT,ERR.ACCOUNT)
	
	IF NOT(R.ACCOUNT) THEN
	AF = FT.DEBIT.ACCT.NO
		ETEXT = "Nomor Rekening Tidak Ada"
		
	CALL STORE.END.ERROR
	
	END

    RETURN
*------------------------------------------------------------------------------
END